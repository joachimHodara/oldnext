#include <benchmark/benchmark.h>

#include "next/next.h"

#include "MyVec.h"

static void BM_SumArray(benchmark::State& state)
{
    size_t size = static_cast<size_t>(state.range(0));
    MyVec<double> x(size, 1.0);
    MyVec<double> y(size, 2.0);
    MyVec<double> result(size);
    while(state.KeepRunning()) {
        for(size_t i = 0; i < size; ++i) {
            result[i] = x[i] + y[i];
        }
        benchmark::DoNotOptimize(result);
    }
}
BENCHMARK(BM_SumArray)->Range(1<<8, 1<<12);

static void BM_SumArrayET(benchmark::State& state)
{
    size_t size = static_cast<size_t>(state.range(0));
    MyVec<double> x(size, 1.0);
    MyVec<double> y(size, 2.0);
    MyVec<double> result(size);
    while(state.KeepRunning()) {
        result = x + y;
        benchmark::DoNotOptimize(result);
    }
}
BENCHMARK(BM_SumArrayET)->Range(1<<8, 1<<12);

BENCHMARK_MAIN();
