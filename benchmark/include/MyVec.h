#pragma once

#include <initializer_list>
#include <iostream>
#include <vector>

#include "next/next.h"

template <typename T>
class MyVec : public next::base_expr<MyVec<T>>
{
public:

    using value_type = T;

    MyVec(std::size_t length) : data_(length) {}

    MyVec(std::size_t length, value_type val) : data_(length, val) {}

    MyVec(const std::initializer_list<T>& list) : data_(list) {}

    std::size_t size() const { return data_.size(); }

    value_type  operator[](std::size_t i) const { return data_[i]; }
    value_type& operator[](std::size_t i) { return data_[i]; }

    template <typename other_expr>
    MyVec& operator=(const next::base_expr<other_expr>& expr)
    {
        next::is_assignable_to(expr, *this);
        for (std::size_t i = 0; i < size(); ++i) {
            data_[i] = expr[i];
        }
        return *this;
    }

    bool operator==(const MyVec& v) const { return data_ == v.data_; }

private:
    std::vector<value_type> data_;
};
