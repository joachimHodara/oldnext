FROM debian:10
MAINTAINER Joachim Hodara <joachim.hodara@gmail.com>

RUN apt-get update \
 && apt-get install -y --no-install-recommends ca-certificates git build-essential clang cmake ninja-build clang-tidy clang-tools \
 && update-ca-certificates

WORKDIR ~/Download/
RUN git clone --depth=1 https://github.com/llvm/llvm-project.git \
 && mkdir libcxx_msan && cd libcxx_msan \
 && cmake -G Ninja ../llvm-project/llvm -DCMAKE_INSTALL_PREFIX=/usr/local -DLLVM_ENABLE_PROJECTS="libcxx;libcxxabi" -DCMAKE_BUILD_TYPE=Release -DLLVM_USE_SANITIZER=Memory -DCMAKE_C_COMPILER=clang -DCMAKE_CXX_COMPILER=clang++ \
 && ninja install-cxx install-cxxabi \
# && ninja check-cxx check-cxxabi \
 && rm -rf ~/Download/llvm-project ~/Download/libcxx_msan

