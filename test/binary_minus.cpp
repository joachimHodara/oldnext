#include "gtest/gtest.h"

#include "next/next.h"

#include "MyVec.h"

TEST(binary_minus, simple)
{
    MyVec<double> allMinusTwos = { -2.0, -2.0, -2.0 };
    MyVec<double> allMinusOnes = { -1.0, -1.0, -1.0 };
    MyVec<double> allZeros = { 0.0, 0.0, 0.0 };
    MyVec<double> allOnes = { 1.0, 1.0, 1.0 };
    MyVec<double> allTwos = { 2.0, 2.0, 2.0 };
    MyVec<double> allThrees = { 3.0, 3.0, 3.0 };
    MyVec<double> allFours = { 4.0, 4.0, 4.0 };

    MyVec<double> result0(allOnes.size());
    result0 = allTwos - allOnes;
    EXPECT_EQ(result0, allOnes);

    MyVec<double> result1(allOnes.size());
    result1 = allZeros - allTwos;
    EXPECT_EQ(result1, allMinusTwos);

    MyVec<double> v1 = { 1.0, 2.0, 3.0 };
    MyVec<double> result2(allOnes.size());
    result2 = v1 - v1;
    EXPECT_EQ(result2, allZeros);

    MyVec<double> v2 = { -3.0, -2.0, -1.0 };
    MyVec<double> result3(allZeros.size());
    result3 = v1 - v2;
    EXPECT_EQ(result3, allFours);
}

TEST(binary_minus, terminal)
{
    MyVec<double> allMinusTwos = { -2.0, -2.0, -2.0 };
    MyVec<double> allMinusOnes = { -1.0, -1.0, -1.0 };
    MyVec<double> allZeros = { 0.0, 0.0, 0.0 };
    MyVec<double> allOnes = { 1.0, 1.0, 1.0 };
    MyVec<double> allTwos = { 2.0, 2.0, 2.0 };
    MyVec<double> allThrees = { 3.0, 3.0, 3.0 };
    MyVec<double> allFours = { 4.0, 4.0, 4.0 };

    MyVec<double> result0(allOnes.size());
    result0 = allOnes - 1.0;
    EXPECT_EQ(result0, allZeros);

    MyVec<double> result1(allOnes.size());
    result1 = 4.0 - allTwos;
    EXPECT_EQ(result1, allTwos);
}

TEST(binary_minus, terminal_conversion)
{
    MyVec<double> allMinusTwos = { -2.0, -2.0, -2.0 };
    MyVec<double> allMinusOnes = { -1.0, -1.0, -1.0 };
    MyVec<double> allZeros = { 0.0, 0.0, 0.0 };
    MyVec<double> allOnes = { 1.0, 1.0, 1.0 };
    MyVec<double> allTwos = { 2.0, 2.0, 2.0 };
    MyVec<double> allThrees = { 3.0, 3.0, 3.0 };
    MyVec<double> allFours = { 4.0, 4.0, 4.0 };

    MyVec<double> result0(allOnes.size());
    result0 = allThrees - 1;
    EXPECT_EQ(result0, allTwos);

    MyVec<double> result1(allOnes.size());
    result1 = 2 - allMinusTwos;
    EXPECT_EQ(result1, allFours);
}

TEST(binary_minus, combined)
{
    MyVec<double> allMinusTwos = { -2.0, -2.0, -2.0 };
    MyVec<double> allMinusOnes = { -1.0, -1.0, -1.0 };
    MyVec<double> allZeros = { 0.0, 0.0, 0.0 };
    MyVec<double> allOnes = { 1.0, 1.0, 1.0 };
    MyVec<double> allTwos = { 2.0, 2.0, 2.0 };
    MyVec<double> allThrees = { 3.0, 3.0, 3.0 };
    MyVec<double> allFours = { 4.0, 4.0, 4.0 };

    MyVec<double> result0(allOnes.size());
    result0 = allOnes - allOnes - allTwos - allMinusOnes;
    EXPECT_EQ(result0, allMinusOnes);

    MyVec<double> v1 = { 0.0, -1.0, -2.0 };
    MyVec<double> v2 = { 3.0, 2.0, 1.0 };
    MyVec<double> result1(allOnes.size());
    result1 = v2 - v1 - allOnes;
    EXPECT_EQ(result1, allTwos);

    MyVec<double> result2(allOnes.size());
    result2 = 6.0 - allOnes - 4.0 - allMinusTwos;
    EXPECT_EQ(result2, allThrees);
}

TEST(binary_minus, combined_conversion)
{
    MyVec<double> allMinusTwos = { -2.0, -2.0, -2.0 };
    MyVec<double> allMinusOnes = { -1.0, -1.0, -1.0 };
    MyVec<double> allZeros = { 0.0, 0.0, 0.0 };
    MyVec<double> allOnes = { 1.0, 1.0, 1.0 };
    MyVec<double> allTwos = { 2.0, 2.0, 2.0 };
    MyVec<double> allThrees = { 3.0, 3.0, 3.0 };
    MyVec<double> allFours = { 4.0, 4.0, 4.0 };

    MyVec<double> result0(allOnes.size());
    result0 = 8 - allOnes - 4.0 - 2;
    EXPECT_EQ(result0, allOnes);

    MyVec<double> result1(allOnes.size());
    result1 = 8.0 - allOnes - 4 - 2.0;
    EXPECT_EQ(result1, allOnes);

    MyVec<double> result2(allOnes.size());
    result2 = 8 - allOnes - 4 - 2;
    EXPECT_EQ(result2, allOnes);
}
