﻿#include "gtest/gtest.h"

#include "next/next.h"

#include "MyVec.h"

TEST(binary_plus, simple) 
{
    MyVec<double> allZeros  = {0.0, 0.0, 0.0};
    MyVec<double> allOnes   = {1.0, 1.0, 1.0};
    MyVec<double> allTwos   = {2.0, 2.0, 2.0};
    MyVec<double> allThrees = {3.0, 3.0, 3.0};
    MyVec<double> allFours  = {4.0, 4.0, 4.0};

    MyVec<double> result0(allOnes.size());
    result0 = allOnes + allTwos;
	EXPECT_EQ( result0, allThrees );

    MyVec<double> result1(allOnes.size());
    result1 = allZeros + allThrees;
    EXPECT_EQ( result1, allThrees );

    MyVec<double> v1  = {0.0, 1.0, 2.0};
    MyVec<double> v2  = {3.0, 2.0, 1.0};
    MyVec<double> result2(allOnes.size());
    result2 = v1 + v2;
    EXPECT_EQ( result2, allThrees );

    MyVec<double> v3 = {0.0, -1.0, -2.0};
    MyVec<double> result3(allZeros.size());
    result3 = v1 + v3;
    EXPECT_EQ( result3, allZeros );
}

TEST(binary_plus, terminal) 
{
    MyVec<double> allZeros  = {0.0, 0.0, 0.0};
    MyVec<double> allOnes   = {1.0, 1.0, 1.0};
    MyVec<double> allTwos   = {2.0, 2.0, 2.0};
    MyVec<double> allThrees = {3.0, 3.0, 3.0};
    MyVec<double> allFours  = {4.0, 4.0, 4.0};

    MyVec<double> result0(allOnes.size());
    result0 = allOnes + 1.0;
	EXPECT_EQ( result0, allTwos );

    MyVec<double> result1(allOnes.size());
    result1 = 2.0 + allTwos;
	EXPECT_EQ( result1, allFours );
}

TEST(binary_plus, terminal_conversion) 
{
    MyVec<double> allZeros  = {0.0, 0.0, 0.0};
    MyVec<double> allOnes   = {1.0, 1.0, 1.0};
    MyVec<double> allTwos   = {2.0, 2.0, 2.0};
    MyVec<double> allThrees = {3.0, 3.0, 3.0};
    MyVec<double> allFours  = {4.0, 4.0, 4.0};

    MyVec<double> result0(allOnes.size());
    result0 = allOnes + 1;
	EXPECT_EQ( result0, allTwos );

    MyVec<double> result1(allOnes.size());
    result1 = 2 + allTwos;
	EXPECT_EQ( result1, allFours );
}

TEST(binary_plus, combined) 
{
    MyVec<double> allZeros  = {0.0, 0.0, 0.0};
    MyVec<double> allOnes   = {1.0, 1.0, 1.0};
    MyVec<double> allTwos   = {2.0, 2.0, 2.0};
    MyVec<double> allThrees = {3.0, 3.0, 3.0};
    MyVec<double> allFours  = {4.0, 4.0, 4.0};

    MyVec<double> result0(allOnes.size());
    result0 = allOnes + allOnes + allOnes + allZeros;
    EXPECT_EQ( result0, allThrees );

    MyVec<double> v1  = {0.0, 1.0, 2.0};
    MyVec<double> v2  = {3.0, 2.0, 1.0};
    MyVec<double> result1(allOnes.size());
    result1 = v1 + v2 + allZeros;
    EXPECT_EQ( result1, allThrees );

    MyVec<double> result2(allOnes.size());
    result2 = 2.0 + allZeros + 1.0 + 1.0;
	EXPECT_EQ( result2, allFours );
}

TEST(binary_plus, combined_conversion) 
{
    MyVec<double> allZeros  = {0.0, 0.0, 0.0};
    MyVec<double> allOnes   = {1.0, 1.0, 1.0};
    MyVec<double> allTwos   = {2.0, 2.0, 2.0};
    MyVec<double> allThrees = {3.0, 3.0, 3.0};
    MyVec<double> allFours  = {4.0, 4.0, 4.0};

    MyVec<double> result0(allOnes.size());
    result0 = 2 + allZeros + 1.0 + 1;
	EXPECT_EQ( result0, allFours );

    MyVec<double> result1(allOnes.size());
    result1 = 2.0 + allZeros + 1 + 1.0;
	EXPECT_EQ( result1, allFours );

    MyVec<double> result2(allOnes.size());
    result2 = 2 + allZeros + 1 + 1;
	EXPECT_EQ( result2, allFours );
}
