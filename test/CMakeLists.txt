cmake_minimum_required(VERSION 3.12)

add_subdirectory("${PROJECT_SOURCE_DIR}/extern/google-test" "extern/google-test")

mark_as_advanced(
    BUILD_GMOCK BUILD_GTEST BUILD_SHARED_LIBS
    gmock_build_tests gtest_build_samples gtest_build_tests
    gtest_disable_pthreads gtest_force_shared_crt gtest_hide_internal_symbols
)

set_target_properties(gtest PROPERTIES FOLDER extern)
set_target_properties(gtest_main PROPERTIES FOLDER extern)
set_target_properties(gmock PROPERTIES FOLDER extern)
set_target_properties(gmock_main PROPERTIES FOLDER extern)

macro(package_add_test TESTNAME)
    add_executable(${TESTNAME} ${ARGN})
    target_link_libraries(${TESTNAME} PRIVATE NExT gtest gmock gtest_main)
    target_include_directories(${TESTNAME} PRIVATE include)
    target_compile_options(${TESTNAME} 
        PRIVATE
            $<$<OR:$<CXX_COMPILER_ID:Clang>,$<CXX_COMPILER_ID:AppleClang>,$<CXX_COMPILER_ID:GNU>>:-pedantic-errors -Wall -Wextra -Wconversion -Wsign-conversion -Wshadow -Wnull-dereference -Wnon-virtual-dtor -Wunused -Wcast-align -Wold-style-cast -Woverloaded-virtual>
            $<$<CXX_COMPILER_ID:GNU>:-Wmisleading-indentation>
            $<$<CXX_COMPILER_ID:MSVC>:/WX /W4 $<IF:$<CONFIG:Debug>,/MTd,/MT>>
    )
    add_test(NAME ${TESTNAME} COMMAND ${TESTNAME})
    set_target_properties(${TESTNAME} PROPERTIES FOLDER test)
endmacro()

package_add_test(binary_plus binary_plus.cpp)
package_add_test(binary_minus binary_minus.cpp)
package_add_test(binary_combined binary_combined.cpp)
