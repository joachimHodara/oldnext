#include "gtest/gtest.h"

#include "next/next.h"

#include "MyVec.h"

TEST(binary_combined, plus_minus)
{
    MyVec<double> allZeros = { 0.0, 0.0, 0.0 };
    MyVec<double> allOnes = { 1.0, 1.0, 1.0 };
    MyVec<double> allTwos = { 2.0, 2.0, 2.0 };
    MyVec<double> allThrees = { 3.0, 3.0, 3.0 };
    MyVec<double> allFours = { 4.0, 4.0, 4.0 };

    MyVec<double> result0(allOnes.size());
    result0 = allOnes + allFours - allTwos;
    EXPECT_EQ(result0, allThrees);

    MyVec<double> result1(allOnes.size());
    result1 = allZeros - allOnes + allThrees + allTwos;
    EXPECT_EQ(result1, allFours);

    MyVec<double> v1 = { 0.0, 1.0, 2.0 };
    MyVec<double> v2 = { 3.0, 2.0, 1.0 };
    MyVec<double> result2(allOnes.size());
    result2 = v1 + v2 - allThrees;
    EXPECT_EQ(result2, allZeros);
    
    MyVec<double> result3(allOnes.size());
    result3 = allZeros - 1.0 + allThrees + 2;
    EXPECT_EQ(result3, allFours);
}
