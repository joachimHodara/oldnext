#pragma once

#include <cstddef>
#include <type_traits>

#include "base_expr.h"
#include "terminal.h"

namespace next {

/**
 * \brief A binary expression
 *
 * The binary expression contains two sub-expressions and a binary operator to evaluate the overall expression.
 */
template <typename left_expr, typename binary_op, typename right_expr>
struct binary_expr : base_expr<binary_expr<left_expr, binary_op, right_expr>>
{
    using value_type = std::common_type_t<typename left_expr::value_type, typename right_expr::value_type>;

    /**
     * \brief Construct a new binary expression. The operator is passed as a template argument
     * \param lhs The left sub-expression
     * \param rhs The right sub-expression
     */
    explicit binary_expr(left_expr lhs, right_expr rhs) : lhs_(std::forward<left_expr>(lhs)), rhs_(std::forward<right_expr>(rhs)) {}

    /**
     * \brief Evaluate the expression for a particular index
     * \param i the index to be evaluated
     */
    value_type operator[](size_t i) const { return binary_op::apply(lhs_[i], rhs_[i]); }

    /**
     * \brief Returns the size of the expression
     */
    size_t size() const { return (std::is_base_of<terminal<typename left_expr::value_type>, left_expr>::value) ? rhs_.size() : lhs_.size(); }

private:
    left_expr lhs_;
    right_expr rhs_;

    static_assert(std::is_base_of<base_expr<left_expr>, left_expr>::value, "Left expression must derive from base expression.");
    static_assert(std::is_base_of<base_expr<right_expr>, right_expr>::value, "Right expression must derive from base expression.");
};

} // end of namespace next
