#pragma once

#include <cstddef>

#include "base_expr.h"

namespace next {

/**
 * \brief A unary expression
 *
 * The unary expression contains one sub-expression and a unary operator to evaluate the overall expression.
 */
template <typename unary_op, typename expr>
struct unary_expr : base_expr<unary_expr<unary_op, expr>>
{
    using value_type = typename expr::value_type;

    /**
     * \brief Construct a new unary expression. The operator is passed as a template argument
     * \param expression The sub-expression
     */
    explicit unary_expr(expr expression) : expression_(std::forward(expression)) {}

    /**
     * \brief Evaluate the expression for a particular index
     * \param i the index to be evaluated
     */
    value_type operator[](size_t i) const { return unary_op::apply(expression_[i]); }

    /**
     * \brief Returns the size of the expression
     */
    size_t size() const { return expression_.size(); }

private:
    expr expression_;

    static_assert(std::is_base_of<base_expr<expr>, expr>::value, "The expression must derive from base expression.");
};

} // end of namespace next

