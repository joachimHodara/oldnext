#pragma once

#include <cstddef>

namespace next {

/**
 * \brief A base class for expressions
 *
 * All expressions derive from base_expr using CRTP.
 */
template <typename Derived>
struct base_expr
{
    /**
     * /brief Returns a reference to the const derived expression
     */
    const Derived& self() const { return static_cast<const Derived&>(*this); }

    /**
     * \brief Evaluate the expression for a particular index
     * \param i the index to be evaluated
     */
    auto operator[](size_t i) const { return self()[i]; }
 
    /**
     * \brief Returns the size of the expression
     */
    size_t size() const { return self().size(); }
};

} // end of namespace next

