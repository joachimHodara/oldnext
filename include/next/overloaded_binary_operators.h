#pragma once

#include <type_traits>

#include "binary_expr.h"
#include "binary_operators.h"
#include "terminal.h"
#include "util.h"

namespace next {

/**
 * \brief Overloading the operator+ for two expressions
 * \param lhs The left expression
 * \param rhs The right expression
 * \return A binary expression with a plus operator
 */
template <typename left_expr, typename right_expr>
auto operator+(const base_expr<left_expr>& lhs, const base_expr<right_expr>& rhs)
{
    check_compatible(lhs, rhs);
    return binary_expr<left_expr, binary_plus, right_expr>(lhs.self(), rhs.self());
}

/**
 * \brief Overloading the operator+ for an expression and an arithmetic value
 * \param lhs An expression
 * \param rhs An arithmetic value
 * \return A binary expression with a plus operator
 */
template <typename left_expr, typename T,
          typename = std::enable_if_t<std::is_arithmetic<T>::value>>
auto operator+(const base_expr<left_expr>& lhs, T rhs)
{
    return binary_expr<left_expr, binary_plus, terminal<T>>(lhs.self(), terminal<T>(rhs));
}

/**
 * \brief Overloading the operator+ for an expression and an arithmetic value
 * \param lhs An arithmetic value
 * \param rhs An expression
 * \return A binary expression with a plus operator
 */
template <typename T, typename right_expr,
          typename = std::enable_if_t<std::is_arithmetic<T>::value>>
auto operator+(T lhs, const base_expr<right_expr>& rhs)
{
    return binary_expr<terminal<T>, binary_plus, right_expr>(terminal<T>(lhs), rhs.self());
}

/**
 * \brief Overloading the operator- for two expressions
 * \param lhs The left expression
 * \param rhs The right expression
 * \return A binary expression with a minus operator
 */
template <typename left_expr, typename right_expr>
auto operator-(const base_expr<left_expr>& lhs, const base_expr<right_expr>& rhs)
{
    check_compatible(lhs, rhs);
    return binary_expr<left_expr, binary_minus, right_expr>(lhs.self(), rhs.self());
}

/**
 * \brief Overloading the operator- for an expression and an arithmetic value
 * \param lhs An expression
 * \param rhs An arithmetic value
 * \return A binary expression with a minus operator
 */
template <typename left_expr, typename T,
          typename = std::enable_if_t<std::is_arithmetic<T>::value>>
auto operator-(const base_expr<left_expr>& lhs, T rhs)
{
    return binary_expr<left_expr, binary_minus, terminal<T>>(lhs.self(), terminal<T>(rhs));
}

/**
 * \brief Overloading the operator- for an expression and an arithmetic value
 * \param lhs An arithmetic value
 * \param rhs An expression
 * \return A binary expression with a minus operator
 */
template <typename T, typename right_expr,
          typename = std::enable_if_t<std::is_arithmetic<T>::value>>
auto operator-(T lhs, const base_expr<right_expr>& rhs)
{
    return binary_expr<terminal<T>, binary_minus, right_expr>(terminal<T>(lhs), rhs.self());
}

} // end of namespace next

