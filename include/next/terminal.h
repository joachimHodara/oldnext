#pragma once

#include <cassert>
#include <cstddef>

#include "base_expr.h"

namespace next {

/**
 * \brief A terminal expression
 *
 * The terminal expression is basically a wrapper around a single value in an expression (vec1 = vec2 + 1.0)
 */
template <typename T>
struct terminal : base_expr<terminal<T>>
{
    using value_type = T;

    /**
     * \brief Construct a new terminal expression
     * \param v Value to be contained in the terminal
     */
    explicit terminal(value_type v) : value(v) {}

    /**
     * \brief Evaluate the expression for a particular index
     * \param i the index to be evaluated
     */
    value_type operator[](size_t i) const 
    { 
        assert(i + 1 != 0);   // This is just to kill the unused argument warning. In C++17, could use [[maybe_unused]] attribute
        return value; 
    }

    /**
     * \brief Returns the size of the expression (1 for terminals)
     */
    size_t size() const { return 1; }

private:
    value_type value;

    static_assert(std::is_arithmetic<T>::value, "T must be an arithmetic type");
};

} // end of namespace next
