#pragma once

namespace next {

/**
 * \brief A plus operator for unary expressions
 */
struct unary_plus
{
    /**
     * \brief Returns the value passed as an input
     * \param t input value
     */
    template <typename T>
    static auto apply(T t) { return t; }
};

} // end of namespace next
