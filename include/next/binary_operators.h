#pragma once

namespace next {

/**
 * \brief A plus operator for binary expressions
 */
struct binary_plus
{
    /**
     * \brief Returns the sum of both input values
     * \param t First input value
     * \param u Second input value
     */
    template <typename T, typename U>
    static auto apply(T t, U u) { return (t + u); }
};

/**
 * \brief A minus operator for binary expressions
 */
struct binary_minus
{
    /**
     * \brief Returns the subtraction of both input values
     * \param t First input value
     * \param u Second input value
     */
    template <typename T, typename U>
    static auto apply(T t, U u) { return (t - u); }
};

} // end of namespace next
