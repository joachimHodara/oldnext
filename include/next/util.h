#pragma once

#include <type_traits>

#include "base_expr.h"
#include "binary_expr.h"
#include "terminal.h"
#include "unary_expr.h"

namespace next {

/**
 * \brief Check whether the expression is a terminal expression or not
 */
template <typename T>
struct is_terminal 
{
    static constexpr bool value = false;
};

template <typename T>
struct is_terminal<terminal<T>>
{
    static constexpr bool value = true;
};

/**
 * \brief Check if the two expressions are compatible. Two expressions are compatible if they
 * have the same size (unless one of them is a terminal) and their types are both arithmetic
 * \param lhs The left expression
 * \param rhs The right expression
 */
template <typename left_expr, typename right_expr>
void check_compatible(const base_expr<left_expr>& lhs, const base_expr<right_expr>& rhs)
{
    // Both expression must have the same size (or one of them must be a terminal)
    assert( (lhs.size() == rhs.size())
         || is_terminal<left_expr>::value
         || is_terminal<right_expr>::value);

    // Both expressions are arithmetic
    assert(std::is_arithmetic<typename left_expr::value_type>::value 
        && std::is_arithmetic<typename right_expr::value_type>::value);
}

/**
 */
template <typename expr, typename T>
void is_assignable_to(const base_expr<expr>& expression, const base_expr<T>& target)
{
    // The expression and target must have the same size, or the expression must be a terminal
    assert((expression.size() == target.size()) || is_terminal<expr>::value);

    // Both expressions are arithmetic
    assert(std::is_arithmetic<typename expr::value_type>::value 
        && std::is_arithmetic<typename T::value_type>::value);
}

} // end of namespace next


